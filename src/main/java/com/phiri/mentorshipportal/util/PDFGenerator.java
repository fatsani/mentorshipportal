package com.phiri.mentorshipportal.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.phiri.mentorshipportal.dal.entities.Meeting;

@Component
public class PDFGenerator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PDFGenerator.class);

	public void generateActivityReport(Meeting meeting, String filePath) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(filePath));
			document.open();
			document.add(generateTable(meeting));
			document.close();
			
		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
			LOGGER.error("Exception in generateActivityReport()"+ e);
		}
	}

	private PdfPTable generateTable(Meeting meeting) {
			PdfPTable pdfPTable = new PdfPTable(2);
			PdfPCell cell;
			cell = new PdfPCell(new Phrase("Meeting Report"));
			cell.setColspan(2);
			pdfPTable.addCell(cell);
			
			cell = new PdfPCell(new Phrase("Meeting Details"));
			cell.setColspan(2);
			pdfPTable.addCell(cell);
			
			pdfPTable.addCell("Meeting Venue");
			pdfPTable.addCell(meeting.getLocationMedium());
						
			pdfPTable.addCell("Meeting Time");
			
			//pdfPTable.addCell(meeting.getStartTime().toString());
			
			pdfPTable.addCell("Meeting Agenda");
			pdfPTable.addCell(meeting.getMeetingAgenda());
			
			pdfPTable.addCell("Meeting ID");
			pdfPTable.addCell(meeting.getMeetingId()+" Id Number");
						
			cell = new PdfPCell(new Phrase("Attending "));
			cell.setColspan(2);
			pdfPTable.addCell(cell);
			
			
			return pdfPTable;
	}
}
