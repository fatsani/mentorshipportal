<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Portal</title>
</head>
<body>
<h2> Portal Section</h2>

<table>

<tr>
<th align='left'>Title</th><td> : ${user.getTitle()}</td>
</tr>
<tr>
<th align='left'>Full Name</th><td> : ${user.getFirstName()} ${user.getSurname()}</td>
</tr>
<tr>
<th align='left'>Email</th><td> : ${user.getEmail()}</td>
</tr>
<tr>
<th align='left'>Mobile</th><td> : ${user.getMobile()}</td>
</tr>
<tr>
<th align='left'>Land line</th><td> : ${user.getLandline()}</td>
</tr>
<tr>
<th align='left'>Business Name</th><td> : ${user.getBusinessName()}</td>
</tr>
<tr>
<th align='left'>Business Category</th><td> : ${user.getBusinessCategory()}</td>
</tr>
<tr>
<th align='left'>Business Status</th><td> : ${user.getBusinessStatus()}</td>
</tr>
<tr>
<th align='left'>Physical Address</th><td> : ${user.getPhysicalAddress()}</td>
</tr>
<tr>
<th align='left'>Postal Address</th><td> : ${user.getPostalAddress()}</td>
</tr>
<tr>
<th align='left'>Birthday</th><td> : ${user.getBirthdate()}</td>
</tr>
<tr>
<th align='left'>BBBEE Status </th><td>: Status</td>

</tr>
<tr>
<th align='left'>Qualifications</th><td>: ${user.getQualifications()}</td>

</tr>


<tr>
<th align='left'><a href="showUpdateUser">Edit Details</a></th>
</tr>
<tr>
<th align='left'><a href="createMeeting">Request Meeting</a></th>
</tr>
<tr>
<th align='left'><a href="displayMeetings">Evaluate Meetings</a></th>
</tr>

	
</table>
</pre>
</form>
</body>
</html>