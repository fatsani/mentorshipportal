package com.phiri.mentorshipportal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.phiri.mentorshipportal.dal.entities.User;
import com.phiri.mentorshipportal.dal.repository.UserRepository;

public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public void deleteUser(User user) {
		userRepository.delete(user);

	}

	@Override
	public User getUserById(int userId) {
		return userRepository.findById((long) userId).orElse(null);
	}

	@Override
	public List<User> getAllUsers() {
		return (List<User>) userRepository.findAll();
	}

}
