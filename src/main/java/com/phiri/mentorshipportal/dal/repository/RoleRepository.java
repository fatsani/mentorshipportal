package com.phiri.mentorshipportal.dal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phiri.mentorshipportal.dal.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
