<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Meetings booked</title>
</head>
<body>

<h2> Meetings</h2>
<table>
<tr>
	<th>Meeting Id</th>
	<th>Creator Id</th>
	<th>Title</th>
	<th>Date and Time</th>
	<th>End Time</th>
	<th>Location / Medium</th>
	
	<th>Meeting Agenda</th>
	<th>Status</th>
	<th>Evaluation</th>
</tr>

<c:forEach items="${ meetings }" var="meeting">
	<tr>
	<td>${meeting.id}</td>
	<td>${meeting.creatorId}</td>
	<td>${meeting.title}</td>
	<td>${meeting.startTime}</td>
	<td>${meeting.endTime}</td>
	<td>${meeting.locationMedium}</td>
	<td>${meeting.meetingAgenda}</td>
	<td>To be evaluated</td>
	
	<td><a href="showEvaluateMeeting">Evaluate</a></td>
	</tr>
	</c:forEach>
	
</table>
</body>
</html>