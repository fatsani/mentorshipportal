package com.phiri.mentorshipportal.dal.repository;

import org.springframework.data.repository.CrudRepository;

import com.phiri.mentorshipportal.dal.entities.User;

public interface UserRepository extends CrudRepository<User, Long>{

	User findByEmail(String email);
	User findById(int id);

}
