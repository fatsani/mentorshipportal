package com.phiri.mentorshipportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MentorshipPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MentorshipPortalApplication.class, args);
	}

}
