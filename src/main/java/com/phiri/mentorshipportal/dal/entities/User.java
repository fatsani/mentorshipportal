package com.phiri.mentorshipportal.dal.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity
@Table(name="usertable")

public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="userid")
	protected int id;
	protected String title;
	@Column(name="firstname")
	protected String firstName;
	protected String surname;
	protected String email;
	protected String password;
	protected String mobile;
	protected String landline;
	@Column(name="businessname")
	protected String businessName;
	@Column(name="businesscategory")
	public BusinessCategory businessCategory = BusinessCategory.OTHER;
	public enum BusinessCategory {AGRICULTURE,MINING,COMMERCE,FINANCE,IT,OTHER};
	@Column(name="businessstatus")
	public BusinessStatus businessStatus = BusinessStatus.OTHER;
	public enum BusinessStatus {REGISTERED,LIQUIDATION,REMOVED,OTHER};
	@Column(name="physicaladdress")
	protected String physicalAddress;
	@Column(name="postaladdress")
	protected String postalAddress;
	
	protected LocalDate birthdate;
	protected String qualifications;
	@ManyToMany
	@JoinTable(name="user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	protected Set<Role> roles;

	public int getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getLandline() {
		return landline;
	}
	public void setLandline(String landline) {
		this.landline = landline;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public BusinessCategory getBusinessCategory() {
		return businessCategory;
	}
	public void setBusinessCategory(BusinessCategory businessCategory) {
		this.businessCategory = businessCategory;
	}
	public BusinessStatus getBusinessStatus() {
		return businessStatus;
	}
	
	public void setBusinessStatus(BusinessStatus businessStatus) {
		this.businessStatus = businessStatus;
	}
	public String getPhysicalAddress() {
		return physicalAddress;
	}
	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public LocalDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}
	public String getQualifications() {
		return qualifications;
	}
	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", title=" + title + ", firstName=" + firstName + ", surname=" + surname + ", email="
				+ email + ", password=" + password + ", mobile=" + mobile + ", landline=" + landline + ", businessName="
				+ businessName + ", businessCategory=" + businessCategory + ", businessStatus=" + businessStatus
				+ ", physicalAddress=" + physicalAddress + ", postalAddress=" + postalAddress + ", birthdate="
				+ birthdate + ", qualifications=" + qualifications + "]";
	}
	
	
	
}
