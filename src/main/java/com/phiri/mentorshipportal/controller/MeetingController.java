package com.phiri.mentorshipportal.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.phiri.mentorshipportal.dal.entities.Meeting;
import com.phiri.mentorshipportal.dal.repository.MeetingRepository;
import com.phiri.mentorshipportal.service.MeetingService;

@Controller
public class MeetingController {
	
	@Autowired
	MeetingService meetingService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MeetingController.class);
	
	@Autowired
	MeetingRepository meetingRepository;
	
	@RequestMapping("/createMeeting")
	public String createMeeting() {
		LOGGER.info("Inside createMeeting()");
		return "createMeeting";
	}
	@RequestMapping("/saveMeeting")
	public String saveMeeting(@ModelAttribute("meeting") Meeting meeting, ModelMap modelMap) {
		LOGGER.info("Inside saveMeeting()");
		Meeting meetingSaved = meetingService.saveMeeting(meeting);
		String confirmationMessage = "Meeting saved : "+meetingSaved.toString();
		modelMap.addAttribute("confirmationMessage", confirmationMessage);
		return "createMeeting";
	}
	
	@RequestMapping("/localdatetime")
    public void dateTime(@RequestParam("localDateTime") 
                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime localDateTime) {
        //Do stuff
    }
	
	@RequestMapping("/displayMeetings")
	public String displayMeetings(ModelMap modelMap) {
		LOGGER.info("Inside displayMeeting()");
		List<Meeting> meetings = meetingService.getAllMeetings();
		modelMap.addAttribute("meetings",meetings);
		return "displayMeetings";
	}
	
	@RequestMapping("/showEvaluateMeeting")
	public String showEvaluateMeeting() {
		LOGGER.info("Inside showEvaluateMeeting()");
		return "evaluateMeeting";
	}
	
	/*@RequestMapping("findMeetings")
	public String findMeetings(@RequestParam("param") String parameter) {
		meetingRepository.findMeetings(parameter);
		return "displayMeetings";
	}
	
	@RequestMapping("findMeetings")
	public String findMeetings(@RequestParam("title") String title, @RequestParam("date") @DateTimeFormat(pattern = "MM-dd-yyyy") LocalDate date, @RequestParam("mentor") String mentor) {
		meetingRepository.findMeetings(title,date,mentor);
		return "displayMeetings";
		
	}*/
	
	
	
}
