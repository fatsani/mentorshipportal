package com.phiri.mentorshipportal;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.phiri.mentorshipportal.dal.entities.Meeting;
import com.phiri.mentorshipportal.dal.entities.User;
import com.phiri.mentorshipportal.dal.entities.User.BusinessCategory;
import com.phiri.mentorshipportal.dal.entities.User.BusinessStatus;
import com.phiri.mentorshipportal.dal.repository.MeetingRepository;
import com.phiri.mentorshipportal.dal.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MentorshipPortalApplicationTests {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MeetingRepository meetingRepository;
/*	
	@Test
	public void testCreateUser() {
		User user = new User();
		user.setTitle("Mr");
		user.setFirstName("Fatsani");
		user.setSurname("Phiri");
		user.setEmail("fphiri@gmail.com");
		user.setPassword("imgood");
		user.setMobile("263771234432");
		user.setLandline("242772430");
		user.setBusinessName("CreativeEng");
		user.setBusinessCategory(BusinessCategory.MINING);
		user.setBusinessStatus(BusinessStatus.REGISTERED);
		user.setPhysicalAddress("123 Odzi Place");
		userRepository.save(user);
		
	}
	@Test
	public void testFindUserById() {
		User user = userRepository.findById(1L).orElse(null);
		System.out.println(user);
	}
	
	@Test
	public void testUpdateUser() {
		User user = userRepository.findById(1L).orElse(null);
		user.setPhysicalAddress("81 Maiden Drive Newlands");
		userRepository.save(user);
	}
	
	@Test
	public void testDeleteUser() {
		User user = userRepository.findById(1L).orElse(null);
		userRepository.deleteById(2L);
		userRepository.delete(user);
	}
*/
	@Test
	public void testCreateMeeting() {
		Meeting meeting = new Meeting();
		meeting.setMeetingId(1);
		meeting.setCreatorId(1);
		meeting.setTitle("Programming");
		meeting.setLocationMedium("1st Floor Auditorium");
		meeting.setMeetingAgenda("SOLID Concepts");
		meeting.setStartTime(LocalDateTime.of(2019, Month.NOVEMBER, 8,9,00));
		meeting.setEndTime(LocalDateTime.of(2019, Month.NOVEMBER, 8,9,30));
		meetingRepository.save(meeting);
	}
	
	@Test
	public void testFindMeetingById() {
		Meeting meeting = meetingRepository.findById(1L).orElse(null);
		System.out.println(meeting);
	}
	
	@Test
	public void testUpdateMeeting() {
		Meeting meeting = meetingRepository.findById(1L).orElse(null);
		meeting.setMeetingAgenda("Desing Patterns");
		meetingRepository.save(meeting);
		
	}
}
