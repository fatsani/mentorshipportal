package com.phiri.mentorshipportal.service;

import java.util.List;

import com.phiri.mentorshipportal.dal.entities.Meeting;
import com.phiri.mentorshipportal.dal.entities.User;

public interface UserService {

	User saveUser(User user);
	
	User updateUser(User user);
	
	void deleteUser(User user);
	
	User getUserById(int userId);
	
	List<User> getAllUsers();
}
