package com.phiri.mentorshipportal.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.phiri.mentorshipportal.dal.entities.Meeting;
import com.phiri.mentorshipportal.dal.repository.MeetingRepository;
import com.phiri.mentorshipportal.dal.repository.UserRepository;
import com.phiri.mentorshipportal.util.EmailUtil;
import com.phiri.mentorshipportal.util.PDFGenerator;

@Service
public class MeetingServiceImpl implements MeetingService {
	@Value("${com.phiri.mentorshipportal.reportdir}")
	private String REPORT_DIR;
	
	@Autowired
	UserRepository userRepository;
	private static final Logger LOGGER = LoggerFactory.getLogger(MeetingServiceImpl.class);

	@Autowired
	private MeetingRepository meetingRepository;
	public MeetingRepository getMeetingRepository() {
		return meetingRepository;
	}

	public void setMeetingRepository(MeetingRepository meetingRepository) {
		this.meetingRepository = meetingRepository;
	}

	@Autowired
	PDFGenerator pdfGenerator;
	
	@Autowired
	EmailUtil emailUtil;
	
	@Override
	public Meeting saveMeeting(Meeting meeting) {
		LOGGER.info("Inside saveMeeting()");
		
		Meeting savedMeeting =  meetingRepository.save(meeting);
		LOGGER.info("Saving the Meeting to the database");
		//
		String filePath = REPORT_DIR+".pdf";
		pdfGenerator.generateActivityReport(meeting, filePath);
		int creatorId = savedMeeting.getCreatorId();
		LOGGER.info("Generating the Meeting report");
		emailUtil.sendReport("phirifatsani@gmail.com", filePath);
		LOGGER.info("Sending the report via email");				
		return savedMeeting;
	}

	@Override
	public Meeting updateLocation(Meeting meeting) {
		return meetingRepository.save(meeting);

	}

	@Override
	public void deleteLocation(Meeting meeting) {
		meetingRepository.delete(meeting);

	}

	@Override
	public Meeting getMeetingById(int meetingId) {
		return meetingRepository.findById((long) meetingId).orElse(null);
	}

	@Override
	public List<Meeting> getAllMeetings() {
		return meetingRepository.findAll();
	}

}
