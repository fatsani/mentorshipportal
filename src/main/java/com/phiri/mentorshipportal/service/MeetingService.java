package com.phiri.mentorshipportal.service;

import java.util.List;

import com.phiri.mentorshipportal.dal.entities.Meeting;

public interface MeetingService {
	
	Meeting saveMeeting(Meeting meeting);
	
	Meeting updateLocation(Meeting meeting);
	
	void deleteLocation(Meeting meeting);
	
	Meeting getMeetingById(int meetingId);
	
	List<Meeting> getAllMeetings();

}
