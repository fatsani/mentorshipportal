package com.phiri.mentorshipportal.dal.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="meeting")
public class Meeting {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	@Column(name="meetingid")
	protected int meetingId;
	@Column(name="creatorid")
	protected int creatorId;
	protected String title;
	@Column(name="locationmedium")
	protected String locationMedium;
	@Column(name="meetingagenda")
	protected String meetingAgenda;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	@Column(name="starttime")
	protected LocalDateTime startTime;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	@Column(name="endtime")
	protected LocalDateTime  endTime;
	//protected int status;
	protected String mentor;


	public Long getId() {
		return id;
	}
	
	public int getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(int meetingId) {
		this.meetingId = meetingId;
	}
	public int getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLocationMedium() {
		return locationMedium;
	}
	public void setLocationMedium(String locationMedium) {
		this.locationMedium = locationMedium;
	}
	public String getMeetingAgenda() {
		return meetingAgenda;
	}
	public void setMeetingAgenda(String meetingAgenda) {
		this.meetingAgenda = meetingAgenda;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	
	/*public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}*/

	@Override
	public String toString() {
		return "Meeting Id :" + id + ", meetingId=" + meetingId + ", Creator Id :" + creatorId + ", Title :" + title
				+ ", Location / Medium :" + locationMedium + ", Agenda :" + meetingAgenda + ", Start Time :" + startTime
				+ ", End Time :" + endTime + " ";
	}
	
}
