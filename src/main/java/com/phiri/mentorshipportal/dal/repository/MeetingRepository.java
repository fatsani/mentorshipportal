package com.phiri.mentorshipportal.dal.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.phiri.mentorshipportal.dal.entities.Meeting;

public interface MeetingRepository extends JpaRepository<Meeting, Long>{
	
	/*@Query("from Meeting where title=: title and startTime=:date and mentor=:mentor")
	List<Meeting> findMeetings(@Param("title") String title, @Param ("date") LocalDate date, @Param ("mentor") String mentor);

	List<Meeting> findMeetings(String parameter);*/ 
	

}
