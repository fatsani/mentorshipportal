package com.phiri.mentorshipportal.util;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.phiri.mentorshipportal.service.MeetingServiceImpl;

@Component
public class EmailUtil {
		
	private String EMAIL_BODY = "Please find report for the Meeting attached";
	private String EMAIL_SUBJECT = "Report for Meeting";
		@Autowired
		private JavaMailSender sender;
		
		private static final Logger LOGGER = LoggerFactory.getLogger(EmailUtil.class);

		public void sendReport(String toAddress, String filePath) {
			LOGGER.info("Inside sendReport()");
			MimeMessage message = sender.createMimeMessage();
			
			try {
				MimeMessageHelper messageHelper = new MimeMessageHelper(message,true);
				messageHelper.setTo(toAddress);
				messageHelper.setSubject(EMAIL_SUBJECT);
				messageHelper.setText(EMAIL_BODY);
				messageHelper.addAttachment("Itinerary", new File(filePath));
				sender.send(message);
			} catch (MessagingException e) {
				LOGGER.error("Exception inside send Report"+ e);
				e.printStackTrace();
			}
		}
}
